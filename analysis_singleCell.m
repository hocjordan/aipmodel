function IRs_tmp = analysis_singleCell(num_cells, num_gripCategories, recallRates, timescalled, save_figures)

    % bins are discrete blocks within the range of firing rates.

    % stimuli are objects. For me, these would be grip categories.

    % transforms are transformations of those objects. For me, these would be
    % the number of different visPatterns that have the same grip i.e. 3.

    % P(r) is the probabiliy that a cell will fire. Calculated via dividing
    % sumPerBin (number of transforms per bin for each cell) by sumPerCell (number of
    % transforms * number of grips).

    % P(s) is the probability that any given stimulus was shown (object for
    % Eguchi)

    % P(r|s) is the probability that r will occur after stimulus. This is calculated via dividing the binMatrix (number of times when the firing rate is classified into a specific
    % bin) by the sumPerObj (represents the number of transforms).

    % have altered program b/c I have only 1D layer of cells not 2D.
    
    multi_cell_analysis = 0;

    %% Declare variables
    
    % get desired folder
    global desiredFolder

    num_transforms = 3;
    num_bins = 10; % can be adjusted?
    %firingRates = zeros(num_gripCategories, num_transforms, num_cells);
    firingRates = reshape(recallRates, num_gripCategories, num_transforms, num_cells);

    binMatrix = zeros(num_cells, num_gripCategories, num_bins); %number of times when fr is classified into a specific bin within a specific objects's transformations
    binMatrixTrans = zeros(num_cells, num_gripCategories, num_bins, num_transforms);  %TF table to show if a certain cell is classified into a certain bin at a certain transformation

    sumPerBin = zeros(num_cells, num_bins);    % transforms per bin for each cell
    sumPerGrip = num_transforms;                        % number of transforms for each grip
    sumPerCell = num_transforms*num_gripCategories;              % total (visual) inputs to a cell

    IRs = zeros(num_cells,num_gripCategories);   %I(R,s) single cell information i.e. the goal of this function
    %pq_r = zeros(num_stimulus);  %prob(s') temporally used in the decoing process
    %Ps = 1/num_stimulus; %Prob(s) 


    %disp('** Data loading **');
    %% Read file and bin the firing rates based on the number of transforms
    for grip = 1:num_gripCategories;
        %disp([num2str(grip) '/' num2str(num_gripCategories)]);
        for permutation = 1:num_transforms;
            for cell = 1:num_cells;
                for bin=1:num_bins
                    if(bin<num_bins)
                        if ((bin-1)*(1/num_bins)<=firingRates(permutation,grip,cell))&&(firingRates(permutation,grip,cell)<(bin)*(1/num_bins)) % have switched permutation and grip around
                            binMatrix(cell,grip,bin)=binMatrix(cell,grip,bin)+1;
                            binMatrixTrans(cell,grip,bin,permutation)=1;
                        end
                    else
                        if ((bin-1)*(1/num_bins)<=firingRates(permutation,grip,cell))&&(firingRates(permutation,grip,cell)<=(bin)*(1/num_bins))
                            binMatrix(cell,grip,bin)=binMatrix(cell,grip,bin)+1;
                            binMatrixTrans(cell,grip,bin,permutation)=1;
                        end
                    end
                end
            end
        end
    end

    %disp('DONE');
    %disp(['** single-cell information analysis **']);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% single-cell information analysis      %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Loop through all cells to calculate single cell information

    for cell=1:num_cells

        % For each cell, count the number of transforms per bin
        for bin=1:num_bins
            sumPerBin(cell,bin)=sum(binMatrix(cell,:,bin));
        end

        % Calculate the information for cell_x cell_y per stimulus
        for grip=1:num_gripCategories
            for bin=1:num_bins
                Pr = sumPerBin(cell,bin)/sumPerCell;
                Prs = binMatrix(cell,grip,bin)/sumPerGrip;
                if(Pr~=0&&Prs~=0)
                    IRs(cell,grip)=IRs(cell,grip)+(Prs*(log2(Prs/Pr)))*((bin-1)/(num_bins-1)); %could be added to weight the degree of firing rates.
                end
            end
        end
    end

    % Order by information content, descending
    IRs_tmp = sort(reshape(max(IRs,[],2),1,num_cells), 'descend');%find max IRs for each 

    % plot and save results
    h = figure();
    plot(IRs_tmp)
    axis([0 num_cells -0.1 log2(num_gripCategories)+0.1])
    
    % save and close figure
    if save_figures == 1
        formatSpec = 'InfoAnalysis%d.fig';
        A1 = timescalled;
        str = sprintf(formatSpec, A1);
        saveas(h, fullfile(desiredFolder, str), 'fig')
        close(h)
    end


%{
    if(multi_cell_analysis == 0)
        dlmwrite([fileName num2str(num_samples*sampleMulti) '_IRs.csv'],IRs_tmp);
        saveas(fig,[fileName '_.png']);
        return;
    end
%}


    %disp('DONE');
end