function [gripPattern, VisNet_firing] = GenerateExemplars(VisNet_firing);

% Generates grip and visual inputs.

%% Vision Exemplars

% retrieve the firing rates of layer 4 from VisNet
%VisNet_firing = VisNet_retrieve();

% Use the variable directly.
% If multiple transformations:
if size(VisNet_firing,3) > 1
    % select a random one for each object
    for transform = 1:size(VisNet_firing,4)
        VisNet_firing_selected{transform} = VisNet_firing(:,:,randi(size(VisNet_firing,3)), transform);
    end
    
    VisNet_firing = zeros(size(VisNet_firing,1), size(VisNet_firing,2), 1, size(VisNet_firing,4));
    for object = 1:size(VisNet_firing_selected,2)
        VisNet_firing(:,:,1,object) = VisNet_firing_selected{object};
    end
end

%% Grip Exemplars
% define a matrix of zeroes

grips = zeros(3,10000);
[rows, columns] = size(grips);

% create 3 exmplars, each one representing a grip using
% 10% of the available neurons, and add them into the 'grips' matrix

for row = 1:rows
    exemplar = randperm(columns, round(0.1*columns));
    grips(row,exemplar) = 1;
end

% slightly alter each of the grip exemplars to obtain 9 grip patterns from the 3
% exemplars

gripPattern = zeros((size(VisNet_firing,4) * size(VisNet_firing,3)),10000);

for count = 0:(size(gripPattern,1)/9)-1
    for row = 1:3
        gripPattern((9*count + 3*row-2),:) = grips(row,:);
        gripPattern((9*count + 3*row-1),:) = grips(row,:);
        gripPattern((9*count + 3*row),:) = grips(row,:);
    end
end

%{
    gripPattern(1) = grips(1);
    gripPattern(2) = grips(1);
    gripPattern(3) = grips(1);
    gripPattern(4) = grips(2);
    gripPattern(5) = grips(3);
    gripPattern(6) = grips(3);
    gripPattern(7) = grips(3);
    gripPattern(8) = grips(1);
    gripPattern(9) = grips(2);
%}



end