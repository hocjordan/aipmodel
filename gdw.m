function [weights] = gdw(outputcells, dilution, VisNet_firing, gripPattern)

    size_vis = (size(VisNet_firing, 2)^2);
    size_grip = size(gripPattern, 2);
    inputcells = size_vis + size_grip;
    
    % generate a nan matrix for future weights
    weights = nan(inputcells, outputcells);

    % generate a normal cumulative distribution, corresponding to the ratio of motor
    % inputs that each output cell should have
    
    distrib = normcdf(1:outputcells, 0.5*outputcells, 0.2*outputcells);
    
    % for each cell:
    
    for cell = 1:outputcells
        
        % generate the cell's probability threshold (motor ratio)
        threshold = randsample(distrib, 1, true);
        
        % create an empty synapse matrix
        
        synapses_Vis = nan(size_vis,1);
        synapses_Grip = nan(size_grip,1);
    
        % generate a random number for each desired synapse
        
        index = rand(1, floor(dilution*inputcells));
        
        % for each of those potential synapses
        
        for count = 1:size(index,2)
            
        
            % if the number is greater than the cell's probability threshold,
            % synapse to a vis cell; if the number is less, synapse to a motor cell
            % check that there are still available cells to link to
            
            if any(isnan(synapses_Vis(:))) == 1 && (index(count) >= threshold || any(isnan(synapses_Grip(:))) == 0)  % link to vis
                newSynapse = randi(size_vis);
                while isnan(synapses_Vis(newSynapse)) ~= 1
                    newSynapse = randi(size_vis);
                end
                synapses_Vis(newSynapse) = rand;
                
            elseif any(isnan(synapses_Grip(:))) == 1 && (index(count) < threshold || any(isnan(synapses_Vis(:))) == 0) % link to grip
                newSynapse = randi(size_grip);
                while isnan(synapses_Grip(newSynapse)) ~= 1
                    newSynapse = randi(size_grip);
                end
                synapses_Grip(newSynapse) = rand;
            end
        end
        
        % combine the synapses for vis and grip to give a final weight
        % array

        weights(:, cell) = [synapses_Vis; synapses_Grip];
    
    end
end