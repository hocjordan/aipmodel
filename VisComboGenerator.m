function merged_stimuli = VisComboGenerator(stimuli)

A = stimuli(:,:,1,1);
B = stimuli(:,:,1,2);
C = stimuli(:,:,1,3);
D = stimuli(:,:,1,4);
E = stimuli(:,:,1,5);
F = stimuli(:,:,1,6);
G = stimuli(:,:,1,7);
H = stimuli(:,:,1,8);
I = stimuli(:,:,1,9);

merged_stimuli = cell(8,9);
count1 = 1;
count2 = 1;

for object1 = ['A','B','C','D','E','F','G','H','I']
    for object2 = ['A','B','C','D','E','F','G','H','I']
        if object1 ~= object2
            
            combined_object = [object1,object2];
            disp(combined_object)
            
            merged = eval(object1) + eval(object2); merged(merged > 1) = 1;
            merged_stimuli{count2,count1} = merged;
            
            
            count2 = count2+1;
        end
        
        
    end
    
    count2 = 1;
    count1 = count1 +1;
    
end
