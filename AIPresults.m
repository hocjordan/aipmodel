function AIPresults(recallRates, label, outputcells, timescalled, save_figures)



    %% sort results by neuron type
    idx = strcmp('V',label);
    find(idx);
    results_V = recallRates(:,idx);
    
    idx = strcmp(label,'M');
    find(idx);
    results_M = recallRates(:,idx);

    idx = strcmp(label,'MV');
    find(idx);
    results_MV = recallRates(:,idx);
    
    idx = strcmp(label,'Mv');
    find(idx);
    results_Mv = recallRates(:,idx);
    
    idx = strcmp(label,'Vm');
    find(idx);
    results_Vm = recallRates(:,idx);
    
    %% sort neuron_type results by firing pattern
    
    results_V = recallRates_sort(3, results_V);
    results_M = recallRates_sort(3, results_M);
    results_MV = recallRates_sort(3, results_MV);
    results_Mv = recallRates_sort(3, results_Mv);
    results_Vm = recallRates_sort(3, results_Vm);
    
    %% matrix graphs
    formatSpec = 'ResultMatrix%d.fig';
    A1 = timescalled;
    str = sprintf(formatSpec, A1);
    
    % set desiredFolder
    global desiredFolder
    
    h = figure();
    subplot(2,3,1)
    imagesc(results_V')
    title('V neurons'); colormap (gray); colorbar;
    
    subplot(2,3,2)
    imagesc(results_M')
    title('M neurons'); colorbar;
    
    subplot(2,3,3)
    imagesc(results_MV'); colorbar; 
    title('MV neurons')
    
    subplot(2,3,4)
    imagesc(results_Vm'); colorbar
    title('Vm neurons')
    
    subplot(2,3,5)
    imagesc(results_Mv'); colorbar
    title('Mv neurons')
    
    % save and close figure
    if save_figures == 1
        saveas(h, fullfile(desiredFolder, str), 'fig')
        close(h)
    end
    
    %% bar graphs
    
    threshold = 0.8;

end

function recallRates_sorted = recallRates_sort(num_gripCategories, recallRates)


    % bins are discrete blocks within the range of firing rates.

    % stimuli are objects. For me, these would be grip categories.

    % transforms are transformations of those objects. For me, these would be
    % the number of different visPatterns that have the same grip i.e. 3.

    % P(r) is the probabiliy that a cell will fire. Calculated via dividing
    % sumPerBin (number of transforms per bin for each cell) by sumPerCell (number of
    % transforms * number of grips).

    % P(s) is the probability that any given stimulus was shown (object for
    % Eguchi)

    % P(r|s) is the probability that r will occur after stimulus. This is calculated via dividing the binMatrix (number of times when the firing rate is classified into a specific
    % bin) by the sumPerObj (represents the number of transforms).

    % have altered program b/c I have only 1D layer of cells not 2D.
    
    multi_cell_analysis = 0;
    save_figures = 0;

    %% Declare variables
    
    % get desired folder
    global desiredFolder

    num_cells = size(recallRates,2);
    num_transforms = 3; % CHANGES DEPENDING ON NO. OF VISUAL INPUTS
    num_bins = 10; % can be adjusted?
    %firingRates = zeros(num_gripCategories, num_transforms, num_cells);
    firingRates = reshape(recallRates, num_gripCategories, num_transforms, num_cells);

    binMatrix = zeros(num_cells, num_gripCategories, num_bins); %number of times when fr is classified into a specific bin within a specific objects's transformations
    binMatrixTrans = zeros(num_cells, num_gripCategories, num_bins, num_transforms);  %TF table to show if a certain cell is classified into a certain bin at a certain transformation

    sumPerBin = zeros(num_cells, num_bins);    % transforms per bin for each cell
    sumPerGrip = num_transforms;                        % number of transforms for each grip
    sumPerCell = num_transforms*num_gripCategories;              % total (visual) inputs to a cell

    IRs = zeros(num_cells,num_gripCategories);   %I(R,s) single cell information i.e. the goal of this function
    %pq_r = zeros(num_stimulus);  %prob(s') temporally used in the decoing process
    %Ps = 1/num_stimulus; %Prob(s) 


    %disp('** Data loading **');
    %% Read file and bin the firing rates based on the number of transforms
    for grip = 1:num_gripCategories;
        %disp([num2str(grip) '/' num2str(num_gripCategories)]);
        for permutation = 1:num_transforms;
            for cell = 1:num_cells;
                for bin=1:num_bins
                    if(bin<num_bins)
                        if ((bin-1)*(1/num_bins)<=firingRates(permutation,grip,cell))&&(firingRates(permutation,grip,cell)<(bin)*(1/num_bins)) % have switched permutation and grip around
                            binMatrix(cell,grip,bin)=binMatrix(cell,grip,bin)+1;
                            binMatrixTrans(cell,grip,bin,permutation)=1;
                        end
                    else
                        if ((bin-1)*(1/num_bins)<=firingRates(permutation,grip,cell))&&(firingRates(permutation,grip,cell)<=(bin)*(1/num_bins))
                            binMatrix(cell,grip,bin)=binMatrix(cell,grip,bin)+1;
                            binMatrixTrans(cell,grip,bin,permutation)=1;
                        end
                    end
                end
            end
        end
    end

    %disp('DONE');

    %% Perform sorting of neurons based on firing within 1-3, 4-6, 7-9 categories.
    
    A_neurons = [];
    B_neurons = [];
    C_neurons = [];
    otherNeurons = [];
    

    % for set ofmatrix data (x = neuron, y = vispattern)
    for cell = 1 : size(binMatrix(:,:,10),1)

        % determine the pattern for which they fire optimally

        %index = find(recallRates(:,cell)==max(recallRates(:,cell)))
        
        % if this pattern is 1,2,3 only then assign it to matrix (A_neurons)

        if binMatrix(cell,1,10) > 0 && binMatrix(cell,2,10) == 0 && binMatrix(cell,3,10)==0 
        A_neurons = [A_neurons,recallRates(:,cell)];

        % if 4,5,6 then B_neurons

        elseif binMatrix(cell,1,10) == 0 && binMatrix(cell,2,10) > 0 && binMatrix(cell,3,10)==0 
            B_neurons = [B_neurons,recallRates(:,cell)];

        % if 7,8,9 then C_neurons

        elseif binMatrix(cell,1,10) == 0 && binMatrix(cell,2,10) == 0 && binMatrix(cell,3,10) > 0 
            C_neurons = [C_neurons,recallRates(:,cell)];

        % all others go in otherNeurons
        else
            otherNeurons = [otherNeurons,recallRates(:,cell)];
        end
    end
            
        % concatenate these matrices back together again
        recallRates_sorted = [A_neurons,B_neurons,C_neurons,otherNeurons];
end 