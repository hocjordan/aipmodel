function AIP_statespace(outputcells)

% Base folder to save simulations
baseFolder = '~/Documents/MATLAB/Simulations/AIPmodel diluted';

% Parameter values to iterate over.
learningRate_statespace = [0.0064, 0.0128, 0.0256, 0.0512, 0.1024, 0.2048, 0.4096, 0.8192]
sparseness_statespace = [10, 20, 30, 40, 50, 60, 70, 80, 90, 99]
dilution_statespace = [0,0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

sumIRs_all = zeros(1,size(learningRate_statespace,2) * size(sparseness_statespace,2) * size(dilution_statespace,2));
count = 0;
global desiredFolder

% permission to make folders if they don't exist?
make = 1;

% calls AIP model many times, altering learning rate

for LR = 1:size(learningRate_statespace,2);
    
    learningRate = learningRate_statespace(LR);
    
    % for each learning rate, alter the firing threshold for competition
    
    for SP = 1:size(sparseness_statespace,2);
        
        sparseness = sparseness_statespace(SP);
        
        % for each firing threshold, alter the dilution
        
        for DL = 1:size(dilution_statespace,2);
            
            dilution = dilution_statespace(DL);
            
            % for each simulation, select a sub-folder to save the results
            % in according to the parameters being used for that
            % simulation.
            
            str = sprintf('LR_%g SP_%g DL_%g', learningRate, sparseness, dilution);
            desiredFolder = fullfile(baseFolder, str);
            
            % if the folder doesn't exist, create it
            
            if make == 1
                if ~exist(desiredFolder, 'dir')
                    mkdir(desiredFolder)
                end
            end
            
            % run simulation
            count = count + 1;
            disp(str)
            IRs_tmp = AIPmodel(outputcells,dilution,sparseness,learningRate);
            
            % thresholds simulation information values and stores them
            index = find(IRs_tmp > 1.4);
            disp(sum(IRs_tmp(index)))
            sumIRs_all(count) = sum(IRs_tmp(index));
            
        end
    end
end

save([baseFolder '/sumIRs_all'], 'sumIRs_all')

end