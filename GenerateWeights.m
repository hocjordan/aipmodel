function [weights] = GenerateWeights(outputcells, VisNet_firing, gripPattern, dilution)

% get the size of the input layer
    size_vis = (size(VisNet_firing, 2)^2);
    size_grip = size(gripPattern, 2);
    inputcells = size_vis + size_grip;
    
    %defines matrix of random values as synapse weights and an initial
    %matrix to record the synapses which are pruned
    weights=rand(inputcells,outputcells);
    %pruned = zeros(round(dilution*inputcells),outputcells);
    
    for column = (1:outputcells);    %for each column
        
        % create an array of unique random numbers that are within the
        % range of synapses; the amount created depends on the set dilution
        dilute = randperm(inputcells,round(dilution*inputcells));
        
        %uses these to replace some of the synapse weights with NaN, giving
        %diluted connectivity
        weights(dilute,column)=NaN;
        
        %records the pruned syanpses
        %pruned(1:end,column) = dilute';
    end
    
end