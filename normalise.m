
function matrix = normalise(matrix)

matrix = matrix ./ repmat( sum(matrix), size(matrix, 1), 1);

end


%% Legacy Code for Normalise Function (very slow)

%{
function y = normalise(matrix)

% Normalise matrix so that each column's sum approaches 1. Ignores NaN.

%get number of rows and columns in matrix

[rows, columns] = size(matrix);

% sum each column
summed = nansum(matrix);
for column = 1:columns
    %divide each row in that column by that sum
    for row = 1:rows
        matrix(row,column) = (matrix(row,column)/summed(column));
    end
end
y = matrix;

end
%}