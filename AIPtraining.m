function weight = AIPtraining(VisNet_firing, gripPattern, weight, outputCells, sparseness, slope, learningRate)

% trains the AIP cells with visPattern and gripPattern


    % for each object
    for object = 1:size(VisNet_firing,4)
  
        % for each transform
        %for transform = 1:size(VisNet_firing,3)
        
            transform = 1; % DELETE
            
            visPattern = VisNet_firing(:,:,transform,object); % replace 3 with transform
            visPattern = visPattern(:)';
            
            inputPattern = [visPattern, gripPattern(object,:)];

            % create a copy of the weight matrix with 0 instead of NaN
            
            noNaNweights=weight;
            noNaNweights(isnan(noNaNweights)) = 0;
            
            % calculate firing rates
            
            firingRates=dot(noNaNweights,(repmat(inputPattern',1,outputCells)));
            
            weight;
            firingRates;
            
            % calculate the effect of soft competition using a threshold
            % sigmoid transfer function. Sparseness between 0 and 100.

            firingRates = softCompetition(sparseness, slope, firingRates);
            
            firingRates;
            
            weight=weight+(learningRate*(inputPattern'*firingRates));
            
            
            % plot the dot product
            %{
            subplot(1,2,1)
            plot(visPattern*weight_Vis)
            title('Vision')

            subplot(1,2,2)
            plot([gripPattern(3,:);gripPattern(1,:);gripPattern(1,:);gripPattern(1,:);gripPattern(2,:);gripPattern(2,:);gripPattern(2,:);gripPattern(3,:);gripPattern(3,:)]*weight_Grip)
            title('Grip')
            %}
            
            weight=normalise(weight);
            
            
            
        %end
    end
end