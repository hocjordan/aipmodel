function label = AIPlabel(weights, outputcells, visPattern, gripPattern)    
% labels each neuron according to the synapses pruned
    label = {};
    
    
    %calculates the proportion of pruned visual and motor synapses
    for column = (1:outputcells);    %for each output neuron
        pruned = find(isnan(weights(:,column)));
        prunedVis = numel(find(pruned <= size(visPattern,2))); %find the no. of pruned visual synapses
        prunedGrip = numel(find(pruned > size(visPattern,2))); %find the no. of pruned motor synapses
        prunedVis = prunedVis/size(visPattern,2); %normalise them
        prunedGrip = prunedGrip/size(gripPattern,2);
        proportion = prunedVis/prunedGrip;

        if or(prunedGrip == prunedVis, abs(proportion-1)<0.1)
            label{column} = 'MV';
        elseif prunedVis == 1
            label{column} = 'M';
        elseif prunedGrip == 1
            label{column} = 'V';
        elseif proportion < 1
            label{column} = 'Vm';
        else
            label{column} = 'Mv';
        end
    end
end