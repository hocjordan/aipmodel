function IRs_tmp = AIPmodel(VisNet_firing, num_outputcells, dilution_level, sparseness, learningRate)

% Actually runs the AIP simulations, taking:

% VisNet Firing: from the VisNet_retrieve function.

% Num Outputcels, which is the number of cells in the competitive layer.

% Dilution Level, which is the proportion of the input cells that each cell
% in the competitive layer is connected to. 1 is almost always preferable.

% Sparseness, an optional parameter that specifies the approximate
% percentage of cells that will be inhibited in the competitive layer.

% LearningRate, an optional parameter that determines how fast the synapses of the competitive
% cells change.

% When running a statespace simulation, the last two parameters will be
% overwritten.



    % running a statespace simulation?
    statespace = 0;
    % set the parameters for competition
    if statespace == 0
        sparseness = 70
        learningRate=0.0016
    end
    
    slope = 90;
    
    % use cumulative dilution model? Other needs fixing

    cumulative = 0;
    
    % choose the desired folder to save results
    global desiredFolder
    desiredFolder = fullfile('~/Documents/MATLAB/Simulations/AIPmodel_VisNet_040314', '1', '1');
        
    % if the folder doesn't exist, ask permission to create it
    make = 0;
    if make == 1
        if ~exist(desiredFolder, 'dir')    
            mkdir(desiredFolder)
        end
    end
    
    %% Generates distributed visual and grip inputs (can later be randomised to test resilience)
    %disp('*Generating Inputs*')
    [gripPattern, visPattern] = GenerateExemplars(VisNet_firing);
    
        %% Generate More Visual Patterns for Testing (using other transforms to test transform-invariance)
    [~, visPattern_testing] = GenerateExemplars(VisNet_firing);

    %% Generates an appropriately sized weight matrix and a record of the pruned synapses for each neuron
    num_inputcells = (size(visPattern,2)^2) + size(gripPattern,2);
    %disp('*Generating Weights*')
    if cumulative == 1
        [weights] = gdw(num_outputcells,dilution_level,visPattern,gripPattern);
    else
        dilution_level = 1 - dilution_level;
        [weights] = GenerateWeights(num_outputcells, visPattern, gripPattern, dilution_level);
    end
        
        
    %% Labels each neuron according to the synapses pruned and displays those labels
    %disp('*Labelling Neurons*')
    label = AIPlabel(weights,num_outputcells,visPattern,gripPattern);

    %% Tests the untrained network with visual inputs only
    disp('*Testing Untrained Network--Vis*')
    recallRates = AIPtesting(1,visPattern_testing, gripPattern, weights, num_outputcells, sparseness, slope);
    recallRates1 = recallRates;
    if statespace == 1
        AIPresults(recallRates, label, num_outputcells, 1, 1); % graph of untrained results
        analysis_singleCell(num_outputcells,3,recallRates, 1, 1); % save graph of info
    else
        AIPresults(recallRates, label, num_outputcells, 1, 0);
        analysis_singleCell(num_outputcells,3,recallRates, 1, 0); % graph of info
    end
    
    %% Trains each neuron with visPattern and gripPattern as inputs
    for epoch = 1:200
        fprintf('%d/%d',epoch,200)
        disp(' ')
        weights = AIPtraining(visPattern, gripPattern, weights, num_outputcells, sparseness, slope, learningRate);
    end
    


    %% tests the trained network with visual and grip inputs
    disp('*Testing Trained Network--Vis/Grip*')
    recallRates = AIPtesting(0,visPattern_testing, gripPattern, weights, num_outputcells, sparseness, slope);
    recallRates2 = recallRates;
    if statespace == 1
        AIPresults(recallRates, label, num_outputcells, 2, 1); % graph of trained results
        analysis_singleCell(num_outputcells,3,recallRates,2, 1);
    else
        AIPresults(recallRates, label, num_outputcells, 2, 0);
        analysis_singleCell(num_outputcells,3,recallRates,2, 0);
    end

    %% Tests the trained network with visual inputs only
    disp('*Testing Trained Network--Vis*')
    recallRates = AIPtesting(1, visPattern_testing, gripPattern, weights, num_outputcells, sparseness, slope);
    recallRates3 = recallRates;
    if statespace == 1
        AIPresults(recallRates, label, num_outputcells, 3, 1); % graph of trained results
        IRs_tmp = analysis_singleCell(num_outputcells,3,recallRates, 3, 1); % save graph of info and return IRs
    else
        AIPresults(recallRates, label, num_outputcells, 3, 0);
        IRs_tmp = analysis_singleCell(num_outputcells,3,recallRates, 3, 0); % graph of info and return IRs
    end

    %% tests the trained network with motor inputs only
    disp('*Testing Trained Network--Grip*')
    recallRates = AIPtesting(2, visPattern_testing, gripPattern, weights, num_outputcells, sparseness, slope);
    recallRates4 = recallRates;
    if statespace == 1
        AIPresults(recallRates, label, num_outputcells, 4, 1); % graph of untrained results
        analysis_singleCell(num_outputcells,3,recallRates,4, 1);
    else
        AIPresults(recallRates, label, num_outputcells, 4, 0);
        analysis_singleCell(num_outputcells,3,recallRates,4, 0);
    end

    %% Save to file

    if statespace == 1
        % save the inputs
        save(fullfile(desiredFolder, 'gripPattern.mat'), 'gripPattern')
        save(fullfile(desiredFolder, 'VisNet_firing.mat'), 'VisNet_firing')
        save(fullfile(desiredFolder, 'visPattern.mat'), 'visPattern')
        save(fullfile(desiredFolder, 'visPattern_testing.mat'), 'visPattern_testing')

        % save the trained weights
        save(fullfile(desiredFolder, 'weights.mat'), 'weights')

        % untrained & vis
        save(fullfile(desiredFolder, 'recallRates1.mat'), 'recallRates1')
        % trained & all
        save(fullfile(desiredFolder, 'recallRates2.mat'), 'recallRates2')
        % trained & vis
        save(fullfile(desiredFolder, 'recallRates3.mat'), 'recallRates3')
        % trained % grip
        save(fullfile(desiredFolder, 'recallRates4.mat'), 'recallRates4')
    end

    %disp('*DONE*')
end